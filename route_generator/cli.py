#!/usr/bin/env python3

import click
import logging
import time

from route_generator.generators.factory import FactoryRouting

logger = logging.getLogger("RouteGenerator")
ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
@click.command()
@click.option('dburi', '--dburi', envvar='DBURI',
              help="ResifInv database access URI to make select requests on tables network and rph5",
              default="postgresql://resifinv_ro@resif-pgprod.u-ga.fr/resifInv-Prod")
@click.option('stationlevel', '-s', '--station-level', envvar='STATIONLEVEL',
              multiple=True,
              help='Extended network codes for which we should publish all stations. Can be specified mutliple times. (environment variable is STATIONLEVEL with space separated values)')
@click.option('output', '-o', '--output', envvar='OUTFILE', type=click.Path(), required=True)
@click.option('verbose', '-v', is_flag=True, default=False, help="Verbose output")
@click.option('noloop', '-n', '--no-loop', is_flag=True, default=False, help="Run just once and exit")
@click.option('sleep', '-S', '--sleep', type=click.INT, default=3600, help="Seconds to sleep between 2 runs")
@click.option('outformat', '-f', '--format', type=click.Choice(['fdsn', 'eida']), required=True)
def cli(dburi, stationlevel, output, verbose, noloop, sleep, outformat):
    """
    Main command line interface.
    """
    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if noloop:
        FactoryRouting.run(outformat, dburi, stationlevel, output)
    else:
        while True:
            logger.info(f"Run routing to {output}")
            FactoryRouting.run(outformat, dburi, stationlevel, output)
            logger.info("  completed")
            time.sleep(sleep)


if __name__ == "__main__":
    cli()
