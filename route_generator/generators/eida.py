#!/usr/bin/env python3
"""
Prints on stdout arclink-XML routing file according to the contents of fdsnws-station webservice contents.
Uses fdsn-station webservice text output format to get network & station inventories.
Script does not handle "priorities", every network gets #1 priority.
"""
import sys
import logging
from datetime import datetime
import psycopg2
import chevron

from . import BaseRouting

logger = logging.getLogger("EIDARouteGenerator")

class EidaRouting(BaseRouting):
    output_date_format = "%Y-%m-%dT%H:%M:%S"

    # Template au format mustache interprété par la librairie chevron
    template = '''<?xml version="1.0" encoding="utf-8"?>
    <ns0:routing xmlns:ns0="http://geofon.gfz-potsdam.de/ns/Routing/1.0/">
    {{#networks}}
    {{#stations}}
        <ns0:route networkCode="{{code}}" stationCode="{{station}}" locationCode="*" streamCode="*">
                <ns0:station address="https://ws.resif.fr/fdsnws/station/1/query" priority="1" start="{{starttime}}" {{#endtime}}end="{{endtime}}"{{/endtime}}/>
                <ns0:dataselect address="https://{{endpoint}}/fdsnws/dataselect/1/query" priority="1" start="{{starttime}}" {{#endtime}}end="{{endtime}}"{{/endtime}}/>
                <ns0:availability address="https://ws.resif.fr/fdsnws/availability/1/query" priority="1" start="{{starttime}}" {{#endtime}}end="{{endtime}}"{{/endtime}}/>
                {{^ph5}}
                <ns0:timeseries address="https://{{endpoint}}/resifws/timeseries/1/query" priority="1" start="{{starttime}}" {{#endtime}}end="{{endtime}}"{{/endtime}}/>
                <ns0:timeseriesplot address="https://{{endpoint}}/resifws/timeseriesplot/1/query" priority="1" start="{{starttime}}" {{#endtime}}end="{{endtime}}"{{/endtime}}/>
                <ns0:wfcatalog address="https://{{endpoint}}/eidaws/wfcatalog/1/query" priority="1" start="{{starttime}}" {{#endtime}}end="{{endtime}}"{{/endtime}}/>
                {{/ph5}}
        </ns0:route>
    {{/stations}}
    {{/networks}}
    </ns0:routing>
    <!-- generated at {{now}} --> '''

    def run(self):
        """
        Gets metada from resifInv database
        Return a json object in FDSN Routing format
        """
        networks = []
        try:
            with psycopg2.connect(self.dburi) as conn:
                with conn.cursor() as curs:
                    # Phase 1 : lister tous les indentifiants de réseau PH5
                    curs.execute("select distinct(network_id) from rph5")
                    ph5_netids = [ n[0] for n in curs.fetchall()]
                    logger.debug("PH5 networks: %s", ph5_netids)
                    # Phase 2: On récupère tous les réseaux dans un tuple (net_id, code, extcode, starttime, endtime)
                    # Explications de la requêtes :
                    #   CASE / THEN / ELSE / END permet de faire soit un nom de réseau temporaire étendu, soit un nom court (si le réseau est permanent).
                    #   LEFT JOIN permet d'ajouter une colone (qui n'apparait pas dans le SELECT) pour compter le nombre de stations associées à chaque réseau
                    #   On ne veut que les réseaux ayant au moins une station, et pas le réseau 0
                    curs.execute("SELECT n.network_id, n.network as code, CASE WHEN isfinite(n.endtime) THEN n.network||n.start_year ELSE n.network END as extcode, n.starttime, n.endtime from networks as n LEFT JOIN (select network_id, count(*) as stations from station group by 1) as s ON n.network_id=s.network_id  where stations >0 and n.network_id != 0 ORDER BY n.network, n.start_year;")
                    for tup in curs.fetchall():
                        net = {"code": tup[1], "extcode": tup[2], "id": tup[0], "starttime": datetime.strftime(tup[3], self.output_date_format)}
                        if tup[4] != datetime.max:
                            net['endtime'] = datetime.strftime(tup[4], self.output_date_format)
                        # all defaults :
                        if net['id'] in ph5_netids:
                            net['ph5'] = True
                            net['endpoint'] = 'ph5ws.resif.fr'
                        else:
                            net['endpoint'] = 'ws.resif.fr'
                        if net['extcode'] in self.stationlevel:
                            net['stations'] = []
                            curs.execute("select station, starttime, endtime from station where network_id=%s", (net['id'],))
                            for statup in curs.fetchall():
                                net['stations'].append({"station": statup[0],
                                                        "starttime":  datetime.strftime(statup[1], self.output_date_format),
                                                        "endtime":  datetime.strftime(statup[2], self.output_date_format)})
                        else:
                            net['stations'] = {"station":'*'}

                        networks.append(net)
                    logger.debug("All networks: %s", networks)
        except psycopg2.Error as err:
            logger.error('Error connecting to %s', self.dburi)
            logger.error(err)
            return None

        if networks:
            xml_out = chevron.render(self.template, {"networks": networks, "now": datetime.now()})
            logger.debug(xml_out)
            return xml_out
