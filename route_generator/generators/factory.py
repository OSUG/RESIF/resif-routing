#!/usr/bin/env python3
from .eida import EidaRouting
from .fdsn import FdsnRouting


class FactoryRouting():
    """
    Permet de construre les différentes classes de routing
    """
    @classmethod
    def run(cls, outformat, dburi, stationlevel, output):
        """
        On suppose que l'outformat est d'une valeur correcte.
        """

        if outformat == 'fdsn':
            router = FdsnRouting(dburi, stationlevel, output)
        elif outformat == 'eida':
            router = EidaRouting(dburi, stationlevel, output)

        data = router.run()

        # Ecriture de l'output seulement si on a pu générer quelque chose.
        if data:
            with open(output, 'w') as fd:
                fd.write(data)
