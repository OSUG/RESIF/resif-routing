#!/usr/bin/env python3
import json
import logging
from datetime import datetime
import psycopg2
import requests

from . import BaseRouting

logger = logging.getLogger("FDSNRouteGenerator")


class FdsnRouting(BaseRouting):
    """
    Generates the routing information as FDSN json format
    """
    def run(self):
        """
        Gets metada from station webservice and resifInv database
        Return a json object in FDSN Routing format
        """
        output_date_format = "%Y-%m-%dT%H:%M:%S.%fZ"
        #
        # Get list of networks
        try:
            networks = requests.get(
                "https://ws.resif.fr/fdsnws/station/1/query?level=network&format=text"
            )
        except Exception as err:
            logger.error(err)
            return None
        logger.info("Station level for %s", self.stationlevel)

        #
        # create a list of datasets
        #
        datasets = []  # List of dictionaries to output as JSON datasets
        ph5_datasets = []  # List of dictionaries to outpus as JSON datasets (PH5)
        ph5_extended_netcodes = []  # List of extended netcodes published in PH5.
        # First the PH5 networks
        logger.debug("Connecting to %s", self.dburi)
        with psycopg2.connect(self.dburi) as conn:
            with conn.cursor() as curs:
                # Get all PH5 networks from the list in rph5. Dirty, inefficient, but no other way for now.
                curs.execute(
                    "select distinct(network_id) from rph5 where network_id != 0"
                )
                for netid in curs.fetchall():
                    curs.execute(
                        "select network,date_trunc('day',starttime),date_trunc('day',endtime) from networks where network_id=%s",
                        (netid,),
                    )
                    if curs.rowcount == 1:
                        ph5net = curs.fetchone()
                        ph5_datasets.append(
                            {
                                "network": ph5net[0],
                                "starttime": ph5net[1].strftime(output_date_format),
                                "endtime": ph5net[2].strftime(output_date_format),
                                "priority": 1,
                            }
                        )
                        # Ces réseaux ne doivent pas être publiés dans la section datasets du centre de données normal
                        # On fait une liste des codes réseaux étendus pour les exclure lors du traitement des autres réseaux
                        ph5_extended_netcodes.append(
                            f"{ph5net[0]}{ph5net[1].strftime('%Y')}"
                        )
        logger.debug("PH5 networks: %s", ph5_extended_netcodes)

        for network_line in networks.text.split("\n")[1:-1]:
            network = network_line.split("|")
            # construction dégueu de l'identifiant long
            # TODO Utiliser la librairie FdsnExtender
            netid = network[0] + network[-3][:4]
            if netid in ph5_extended_netcodes:
                logger.debug("%s allready published in PH5. Skip it", netid)
                continue
            logger.info("Getting metadata for %s", netid)
            if netid in self.stationlevel:
                logger.info(
                    "  %s needs stations level metadata. Getting the list of the stations",
                    netid,
                )
                stations = requests.get(
                    f"https://ws.resif.fr/fdsnws/station/1/query?level=station&net={network[0]}&starttime={network[-3]}&endtime={network[-2]}&format=text"
                )
                for station_line in stations.text.split("\n")[1:-1]:
                    station = station_line.split("|")
                    starttime = datetime.strptime(
                        station[-2], "%Y-%m-%dT%H:%M:%S"
                    ).strftime(output_date_format)
                    endtime = datetime.strptime(
                        station[-1], "%Y-%m-%dT%H:%M:%S"
                    ).strftime(output_date_format)
                    datasets.append(
                        {
                            "network": station[0],
                            "station": station[1],
                            "starttime": starttime,
                            "endtime": endtime,
                            "priority": 1,
                        }
                    )
            else:
                # We only need to publish the whole network, mark it as done
                starttime = datetime.strptime(
                    network[-3], "%Y-%m-%dT%H:%M:%S"
                ).strftime(output_date_format)
                endtime = datetime.strptime(network[-2], "%Y-%m-%dT%H:%M:%S").strftime(
                    output_date_format
                )
                datasets.append(
                    {
                        "network": network[0],
                        "starttime": starttime,
                        "endtime": endtime,
                        "priority": 1,
                    }
                )

        datacenter = {
            "version": 1,
            "datacenters": [
                {
                    "fullName": "RESIF Data Centre",
                    "name": "RESIF",
                    "summary": "French seismologic and geodetic network",
                    "website": "https://seismology.resif.fr",
                    "repositories": [
                        {
                            "description": "BCSF Rénass events catalog",
                            "name": "event",
                            "services": [
                                {
                                    "description": "Access to events data. Service provided by BCSF-Rénass.",
                                    "name": "fdsnws-event-1",
                                    "url": "https://api.franceseisme.fr/fdsnws/event/1/",
                                },
                            ]
                        },
                        {
                            "description": "SEED based archive of continuous seismological data",
                            "name": "archive",
                            "services": [
                                {
                                    "description": "Access to raw time series data",
                                    "name": "fdsnws-dataselect-1",
                                    "url": "https://ws.resif.fr/fdsnws/dataselect/1/",
                                },
                                {
                                    "description": "Access to metadata describing raw time series data",
                                    "name": "fdsnws-station-1",
                                    "url": "https://ws.resif.fr/fdsnws/station/1/",
                                },
                                {
                                    "description": "Availability of time series data",
                                    "name": "fdsnws-availability-1",
                                    "url": "https://ws.resif.fr/fdsnws/availability/1/",
                                },
                                {
                                    "description": "EIDA WFCatalog service",
                                    "name": "eidaws-wfcatalog",
                                    "url": "https://ws.resif.fr/eidaws/wfcatalog/1/",
                                },
                                {
                                    "description": "Plot of timeseries",
                                    "name": "resifws-timeseriesplot-1",
                                    "url": "https://ws.resif.fr/resifws/timeseriesplot/1/",
                                },
                                {
                                    "description": "Processed and reformatted time series",
                                    "name": "resifws-timeseries-1",
                                    "url": "https://ws.resif.fr/resifws/timeseries/1/",
                                },
                                {
                                    "description": "Metadata in SAC Poles and Zeros format",
                                    "name": "resifws-sacpz-1",
                                    "url": "https://ws.resif.fr/resifws/sacpz/1/",
                                },
                                {
                                    "description": "Metadata in SEED-RESP format",
                                    "name": "resifws-resp-1",
                                    "url": "https://ws.resif.fr/resifws/resp/1/",
                                },
                                {
                                    "description": "Evaluated responses in various formats",
                                    "name": "resifws-evalresp-1",
                                    "url": "https://ws.resif.fr/resifws/evalresp/1/",
                                },
                                {
                                    "description": "Returns Probabilisty Density Functions and spectrograms of seismic data. Generates plots or raw values.",
                                    "name": "resifws-seedpsd-1",
                                    "url": "https://ws.resif.fr/resifws/seedpsd/1/",
                                },
                            ],
                            "datasets": datasets,
                        },
                        {
                            "description": "PH5 based archive",
                            "name": "PH5",
                            "services": [
                                {
                                    "name": "ph5-dataselect-1",
                                    "url": "https://ph5ws.resif.fr/fdsnws/dataselect/1",
                                    "description": "Raw time series data",
                                    "compatibleWith": "fdsnws-dataselect-1",
                                },
                                {
                                    "name": "ph5-availability-1",
                                    "url": "https://ph5ws.resif.fr/resifws/availability/1",
                                    "description": "Availability of timeseries data",
                                },
                            ],
                            "datasets": ph5_datasets,
                        },
                    ],
                }
            ],
        }

        # return the whole object as JSON string
        return json.dumps(datacenter)
