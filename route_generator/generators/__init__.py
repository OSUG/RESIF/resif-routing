#!/usr/bin/env python3

class BaseRouting():
    def __init__(self, dburi, stationlevel, output):
        self.dburi = dburi
        self.stationlevel = stationlevel
        self.output = output
